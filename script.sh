#!/bin/bash

sed 1d batchDetails.txt | while IFS="	" read -r sno name batch tool
do
   echo "${name}'s tool is ${tool}"
done
